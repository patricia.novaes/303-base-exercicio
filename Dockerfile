FROM openjdk:8-jre-alpine
COPY target/Api-Investimentos-0.0.1-SNAPSHOT.jar app-investimentos.jar


# Para usar os argumentos (credenciais do mysql) que foram passados pelo Jenkins como argumentos
# do comando docker.build, precisamos receber esse argumentos dentro do Dockerfile definindo
# através do comando ARG qual o nome de cada argumento passado.

ARG mysql_host
ARG mysql_port
ARG mysql_user
ARG mysql_pass

# Com o bloco comando ENV, definimos que os valores dos argumentos passados no comando docker.build e
# recebidos pelos comandos ARG, serão utilizados para definir variáveis de ambiente que serão acessíveis
# em tempo de execução do container (após do docker run). Por exemplo, dizemos que dentro do container
# existirá uma variável de ambiente chamada mysql_host que armazena o valor passado por argumento através
# do arg $mysql_host.

ENV mysql_host=$mysql_host mysql_port=$mysql_port mysql_user=$mysql_user mysql_pass=$mysql_pass

CMD ["java", "-jar", "app-investimentos.jar"]